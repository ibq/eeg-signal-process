
##
## 程序作用: 19个耳鸣病人的数据进行小波变换后，每个人有192条，程序识别文件名对数据进行病号归类，剪切到
## 目的地址

import numpy as np
import os
import sys
import re
import shutil

path = './'
files = []

files = os.listdir(path) #获取所有文件
count = 0

def move_file(source, target):
	if not os.path.exists(target):
		os.makedirs(target)
	shutil.move(source, target)
	print(source, '----', target)

def extract_number(f):
	number = re.search('(\\d+)', f)
	if(number): #得到文件前缀数字，判断应该是哪一个人的
		people_class = int(number.group())//192 + 1
		target_path = str(people_class)
		source_name = f
		move_file(source_name, target_path)

for file in files:
	if file.endswith('.npy'):
		extract_number(file)
		count += 1
	else:
		continue

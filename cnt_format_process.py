# -*- coding: utf-8 -*-
"""
Created on Fri May  6 19:38:00 2022

@author: liu
"""

import mne
from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
import os
from os import path

def find_mnt_file(find_path):
    filelist = []
    file = os.listdir(find_path)
    for f in file:
        if(f.endswith(".cnt")):
            filelist.append(f)
    return filelist

def main():
    find_path = './'
    filelist = find_mnt_file(find_path)
    s = 1
    for f in filelist:
        raw = mne.io.read_raw_cnt(f, preload=True)
        print("正在处理------------------------------------->>>>", f)
        raw.drop_channels('CB2') #删除不必要的通道
        raw.drop_channels('M1')
        raw.drop_channels('M2')
        raw.drop_channels('CB1')
        raw.filter(0.1, 45)#进行0.1-45Hz的滤波
        raw.resample(200)#进行200Hz的重采样 1000Hz--->200Hz
        arr = raw.get_data(picks = 'all')
        newarray = arr[:,:60800]
        out = newarray.reshape(60, 152, 400)
        out = np.transpose(out, (1, 0, 2))
        if s == 1:
            comp = out
        else:
            comp = np.concatenate((comp, out), axis=0)
        print("处理完毕------------------------------------>>>>", f)
        s += 1
    np.save("no_tinnitus_tmp2", comp)
    
if __name__ == '__main__':
    main()
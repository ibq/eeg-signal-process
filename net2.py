# import os
# import tensorflow as tf
# from keras.backend.tensorflow_backend import set_session
 
# os.environ["CUDA_VISIBLE_DEVICES"] = "0"
# config = tf.ConfigProto() 
# config.gpu_options.allow_growth = True
# config.gpu_options.per_process_gpu_memory_fraction = 0.8
# set_session(tf.Session(config=config))
import sys
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '/device:GPU:0'
from matplotlib.backends.backend_agg import FigureCanvasAgg
import numpy as np
import pywt
from keras.datasets import mnist
from keras.models import Model
from keras.layers import Dense, Input, Conv1D
import matplotlib.pyplot as plt
from keras.models import Sequential

from keras.layers import Dense, Activation
from keras.optimizers import RMSprop

import numpy as np
np.random.seed(1337)  # for reproducibility
from keras.datasets import mnist
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers import Dense, Activation, Convolution2D, MaxPooling1D, Flatten
from keras.optimizers import Adam

from keras.utils import np_utils

data_1 = np.load('tinnitus.npy') #耳鸣患者

x_test_1 = data_1[0:4*192]
#测试集大小
y_test_1 = np.ones((len(x_test_1), 1))

x_train_1 = data_1[4*192:]
#训练集大小
y_train_1 = np.ones((len(x_train_1), 1))

data_2 = np.load('no_tinnuts.npy')
x_test_2 = data_2[0:4*192]
y_test_2 = np.zeros((len(x_test_2), 1))
x_train_2 = data_2[4*192:]
y_train_2 = np.zeros((len(x_train_2), 1))


x_train = np.concatenate((x_train_1,x_train_2))
#x_train = x_train.reshape(1,x_train.shape[0],x_train.shape[1],x_train.shape[2])
y_train = np.concatenate((y_train_1,y_train_2))
y_train = np_utils.to_categorical(y_train, num_classes=2)
# print(y_train)

x_test = np.concatenate((x_test_1,x_test_2))
#x_test = x_test.reshape(1,x_test.shape[0],x_test.shape[1],x_test.shape[2])
y_test = np.concatenate((y_test_1,y_test_2))
y_test = np_utils.to_categorical(y_test, num_classes=2)
#print(y_test)
print(x_train.shape,y_train.shape,x_test.shape,y_test.shape)
# pause()
# testdata = x_train[0,7]
# print("小波变换测试数据大小", len(testdata))
np.save('y_test.npy',y_test)
# pause()
def cwt(testdata):
	wavename = "cgau8"  #小波函数
	totalscal = 256     #totalscal是对信号进行小波变换时所用尺度序列的长度(通常需要预先设定好)
	fc = pywt.central_frequency(wavename)#计算小波函数的中心频率
	cparam = 2 * fc * totalscal  #常数c
	scales = cparam/np.arange(totalscal,1,-1) #为使转换后的频率序列是一等差序列，尺度序列必须取为这一形式（也即小波尺度）

	[cwtmatr, frequencies] = pywt.cwt(testdata,scales,wavename,1.0/200)#连续小波变换模块
	t = np.arange(0,2.0,1.0/200)
	plt.contourf(t, frequencies, abs(cwtmatr))
	plt.ylabel(u"freq(Hz)")
	plt.xlabel(u"time(s)")
	#plt.subplots_adjust(hspace=0.4) #调整边距和子图的间距 hspace为子图之间的空间保留的高度，平均轴高度的一部分
	#print(frequencies,cwtmatr.shape)
	plt.axis('off')
	# plt.gcf().set_size_inches(784 / 100, 784 / 100)
	plt.gca().xaxis.set_major_locator(plt.NullLocator())
	plt.gca().yaxis.set_major_locator(plt.NullLocator())
	plt.subplots_adjust(top=1, bottom=0, right=1, left=0, hspace=0, wspace=0)
	plt.margins(0, 0)
	canvas = FigureCanvasAgg(plt.gcf())
	canvas.draw()
	img = np.array(canvas.renderer.buffer_rgba())
	output = img[:,:,1]
	output = output[240:,0:240]
	output = output.astype(np.int8)
	# plt.imshow(output)
	# plt.show()
	# print(output.shape)
	return output

X_train = np.zeros((90,60,240,240),dtype=np.int8)
#print(X_train.shape)

# s = np.load('D:\\target\\X_train.npy')
# print(s.shape)
space = [15,80,130]

wait = []
for i in range(8):
	for j in range(0, len(space)):
		wait.append(space[j] + i * 192)

print(wait, len(wait))
np.save('wait.npy',wait)
# for i in range(0, len(wait)):
# 	print(i)

# i = int(sys.argv[1])
# print(wait[i])
# tmp = np.zeros((60,240,240))
# for j in range(60):
# 	print('processing  No.',i,'---',j)
# 	tmp[j] = cwt(x_test[wait[i],j])
# stri = 'D:\\target\\picture\\' + str(i) + 'X_test.npy'
# np.save(stri,tmp)

# # print("最终尺寸", X_train.shape)

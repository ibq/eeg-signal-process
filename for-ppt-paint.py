# -*- coding: utf-8 -*-
"""
Created on Thu May 12 09:12:02 2022

@author: liu
"""

import mne
from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
import os
from os import path

# file_path = '04final.set'
# raw = mne.read_epochs_eeglab(file_path,eog=(),uint16_codec=None,verbose=None)
# raw.plot()
# plt.show()
# print(raw.info)

# newraw = raw.filter(0.1, 45)#滤波 范围是(0.1--45)
# newraw.plot()
# plt.show()
# print(newraw.info)

# newraw = newraw.resample(200)#降采样 从2000点降采样到400点
# newraw.plot()
# plt.show()
# print(newraw.info)

file_path2 = '杨鞠，对照组.cnt'
raw = mne.io.read_raw_cnt(file_path2, preload=True)
raw.plot(start=200)
print(raw.info)

raw.drop_channels('CB2') #删除不必要的通道
raw.drop_channels('M1')
raw.drop_channels('M2')
raw.drop_channels('CB1')

raw.filter(0.1, 45)#进行0.1-45Hz的滤波
raw.plot(start=200)
print(raw.info)

raw.resample(200)#进行200Hz的重采样 1000Hz--->200Hz
raw.plot(start=200,duration=10)
print(raw.info)

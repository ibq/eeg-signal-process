# -*- coding: utf-8 -*-
"""
Created on Thu May  5 20:50:58 2022

@author: liu
"""

import mne
from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
import os
from os import path

def find_set_file(find_path):
    filelist = []
    file = os.listdir(find_path)
    for f in file:
        if(f.endswith(".set")):
            filelist.append(f)
    return filelist

def main():
    find_path = './' #要遍历的文件夹
    s = 1
    filelist = find_set_file(find_path)
    for f in filelist:
        #首先读取set文件
        raw = mne.read_epochs_eeglab(f,eog=(),uint16_codec=None,verbose=None)
        print("正在处理------------------------------------->>>>", raw.filename)
        newraw = raw.filter(0.1, 45)#滤波 范围是(0.1--45)
        newraw = newraw.resample(200)#降采样 从2000点降采样到400点
        data = newraw.get_data(picks = 'all') #获取3维数据
        data = data[:152,:,:]
        if s == 1:
            comp = data
        else:
            comp = np.concatenate((comp, data), axis = 0)
        s += 1
        print("处理完毕------------------------------------>>>>", raw.filename)
    np.save("no_tinnitus_tmp", comp)

if __name__ == '__main__':
    main()
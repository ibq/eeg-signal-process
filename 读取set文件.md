读取set文件

```python
raw = mne.read_epochs_eeglab('04final.set',
                       eog=(),  
                       uint16_codec=None,
                       verbose=None)
```

使用滤波器滤波

```python
raw.filter(0.1, 50)
```

查看信号的详细信息

```python
raw.info
```

对信号进行重新采样

```python
newraw = newraw.resample(200)#降采样，采样率
```

将set格式中的数据取出来

```python
test2 = newraw.get_data(picks = 'all')#将所有数据取出来成为一个3D数组
```

查看array数组的大小

```python
test.shape
```

将array转换为numpy数组

```python
arr2 = np.array(data2)#data2为array数组
```

拼接numpy数组

```python
comp = np.concatenate((data, data2), axis = 0)#相当于把数组摞起来
```

numpy数组维度调换

```python
trans = np.transpose(data2,[1, 0, 2])#意思就是将前两个维度大小调换
```

note: 事实上声明一个空的numpy数组是不现实的，压根就没有这种方法

删除某一通道

```python
raw.drop_channels('CB2')
raw.drop_channels('M1')
raw.drop_channels('M2')
raw.drop_channels('CB1')
```

读取cdt数据:

```python
raw = mne.io.read_raw_curry(path, preload=True)#path就是cdt文件的路径
```


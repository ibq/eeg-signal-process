import numpy as np
import os
import sys
import matplotlib.pyplot as plt
import re

path = './'
files = []
for file in os.listdir(path):
	if file.endswith('.npy'):
		files.append(file)
print(files)

# out = np.zeros((24,60,240,240))
# i = 0
# for file in files:
# 	print('processing------', file)
# 	tmp = np.load(file)
# 	out[i] = tmp
# 	i += 1
# print(out.shape)
# np.save('x_test.npy',out)
# print('finish')


number = np.zeros(24)
rule = '(\\d+)'
j = 0
for file in files:
	ret = re.search(rule, file)
	if(ret):
		number[j] = ret.group()
		j += 1
print(number)
number = number.astype(np.int8)
print(number)

y = np.load('y_test.npy')
index = np.load('wait.npy')
print(index,y)

y_label = np.zeros((24,2))
j = 0
for i in number:
	# print(i)
	# print(index[i])
	# print(y[index[i]])
	y_label[j] = y[index[i]]
	j += 1
print(y_label)
np.save('y_test.npy',y_label)
